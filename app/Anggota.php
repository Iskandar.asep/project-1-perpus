<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anggota extends Model
{
    protected $table = 'anggota';
    protected $fillable =['user_id','npm','nama','tempat_lahir','tgl_lahir','jeniskelamin','telepon','prodi'];
}
