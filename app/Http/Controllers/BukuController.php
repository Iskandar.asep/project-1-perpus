<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
<<<<<<< HEAD
use App\User;
use App\Buku;
use Auth;
use DB;
=======

use DB;
use App\Buku;
>>>>>>> e8ad3920a03029df99da6e48053866b29f18e01a

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
<<<<<<< HEAD
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $datas = Buku::get();
        return view('buku.index', compact('datas'));
=======
    public function index()
    {
        $buku = Buku::all();
        return view('buku.index', compact('buku'));
>>>>>>> e8ad3920a03029df99da6e48053866b29f18e01a
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
<<<<<<< HEAD
        $datas = DB::table('buku')->get();
        return view('buku.create', compact('datas'));
    }

    public function format()
    {
        $data = [['judul' => null, 'isbn' => null, 'pengarang' => null, 'penerbit' => null, 'tahun_terbit' => null, 'jumlah_buku' => null, 'deskripsi' => null, 'lokasi' => 'rak1/rak2/rak3']];
            $fileName = 'format-buku';
            

        $export = Excel::create($fileName.date('Y-m-d_H-i-s'), function($excel) use($data){
            $excel->sheet('buku', function($sheet) use($data) {
                $sheet->fromArray($data);
            });
        });

        return $export->download('xlsx');
    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'importBuku' => 'required'
        ]);

        if ($request->hasFile('importBuku')) {
            $path = $request->file('importBuku')->getRealPath();

            $data = Excel::load($path, function($reader){})->get();
            $a = collect($data);

            if (!empty($a) && $a->count()) {
                foreach ($a as $key => $value) {
                    $insert[] = [
                            'judul' => $value->judul, 
                            'isbn' => $value->isbn, 
                            'pengarang' => $value->pengarang, 
                            'penerbit' => $value->penerbit,
                            'tahun_terbit' => $value->tahun_terbit, 
                            'jumlah_buku' => $value->jumlah_buku, 
                            'deskripsi' => $value->deskripsi, 
                            'lokasi' => $value->lokasi,
                            'cover' => NULL];

                    Buku::create($insert[$key]);
                        
                    }
                  
            };
        }
        alert()->success('Berhasil.','Data telah diimport!');
        return back();
=======
        $users = DB::table('buku')->get();
        return view('buku.create', compact('users'));
>>>>>>> e8ad3920a03029df99da6e48053866b29f18e01a
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
<<<<<<< HEAD
        $this->validate($request, [
            'judul' => 'required|string|max:255',
            'isbn' => 'required|string'
        ]);

    //    if($request->file('cover')) {
      //      $file = $request->file('cover');
         //   $dt = Carbon::now();
          //  $acak  = $file->getClientOriginalExtension();
          //  $fileName = rand(11111,99999).'-'.$dt->format('Y-m-d-H-i-s').'.'.$acak; 
          $gambarName = time().'.'.$request->cover->extension();  
        
          $request->cover->move(public_path('images_buku'), $gambarName);
  
  //      } else {
    //        $gambarName = NULL;
      //  }

        Buku::create([
                'judul' => $request->get('judul'),
                'isbn' => $request->get('isbn'),
                'pengarang' => $request->get('pengarang'),
                'penerbit' => $request->get('penerbit'),
                'tahun_terbit' => $request->get('tahun_terbit'),
                'jumlah_buku' => $request->get('jumlah_buku'),
                'deskripsi' => $request->get('deskripsi'),
                'lokasi' => $request->get('lokasi'),
                'cover' => $gambarName
            ]);

        //alert()->success('Berhasil.','Data telah ditambahkan!');

        return redirect()->route('buku.index');

=======
        $request->validate([
            'judul' => 'required',
            'pengarang' => 'required',
            'penerbit' => 'required',
            'tahun_terbit' => 'required',
            'jumlah_buku' => 'required',
            'deskripsi' => 'required',
        ]);

        $buku->judul = $request->judul;
		$buku->pengarang = $request->pengarang;
		$buku->penerbit = $request->penerbit;
		$buku->tahun_terbit = $request->tahun_terbit;
		$buku->jumlah_buku = $request->jumlah_buku;
		$buku->deskripsi = $request->deskripsi;

		$buku->save();
        
        return redirect('/buku');
>>>>>>> e8ad3920a03029df99da6e48053866b29f18e01a
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
<<<<<<< HEAD

        $data = Buku::findOrFail($id);

        return view('buku.show', compact('data'));
=======
        $buku = Buku::findOrFail($id);  
        
        return view('buku.show', compact('buku'));
>>>>>>> e8ad3920a03029df99da6e48053866b29f18e01a
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
<<<<<<< HEAD
    {   

        $data = Buku::findOrFail($id);
        return view('buku.edit', compact('data'));
=======
    {
        //
>>>>>>> e8ad3920a03029df99da6e48053866b29f18e01a
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
<<<<<<< HEAD
        $request->validate([
            'judul' =>'required',
            'isbn' =>'required',
            'pengarang' =>'required',
            'penerbit' =>'required',
            'tahun_terbit' =>'required',
            'jumlah_buku' =>'required',
            'deskripsi' =>'required',
            'lokasi' =>'required',
            'cover' =>'image|mimes:jpg,png,jpeg|max:2048',]);

        $datas = Buku::find($id);

        if($request->has('cover')){


            $posterName = time().'.'.$request->cover->extension();  
            $request->cover->move(public_path('images_buku'), $posterName);
    
            $datas->judul = $request->judul;
            $datas->isbn = $request->isbn;
            $datas->pengarang = $request->pengarang;
            $datas->penerbit = $request->penerbit;
            $datas->tahun_terbit = $request->tahun_terbit;
            $datas->jumlah_buku = $request->jumlah_buku;
            $datas->deskripsi = $request->deskripsi;
            $datas->lokasi = $request->lokasi;
            $datas->cover = $posterName;

            $datas->update();

             return redirect('/film');

        } else {
            $datas = Buku::find($id);
            $datas->judul = $request->judul;
            $datas->isbn = $request->isbn;
            $datas->pengarang = $request->pengarang;
            $datas->penerbit = $request->penerbit;
            $datas->tahun_terbit = $request->tahun_terbit;
            $datas->jumlah_buku = $request->jumlah_buku;
            $datas->deskripsi = $request->deskripsi;
            $datas->lokasi = $request->lokasi;

            $datas->update();

             return redirect('/buku');
        }
=======
        //
>>>>>>> e8ad3920a03029df99da6e48053866b29f18e01a
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
<<<<<<< HEAD
        Buku::find($id)->delete();
       // alert()->success('Berhasil.','Data telah dihapus!');
        return redirect()->route('buku.index');
=======
        //
>>>>>>> e8ad3920a03029df99da6e48053866b29f18e01a
    }
}
