<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\users;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Users::all();
        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = DB::table('users')->get();
        return view('user.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' =>'required',
            'username' =>'required',
            'email' =>'required',
            'password' =>'required',
            'gambar' =>'required|image|mimes:jpg,png,jpeg|max:2048',
            'level' =>'required',
        ]);
        $gambarName = time().'.'.$request->gambar->extension();  
        
        $request->gambar->move(public_path('avatar'), $gambarName);

        $users = new users;

        $users->name = $request->name;
        $users->username = $request->username;
        $users->email = $request->email;
        $users->password = $request->password;
        $users->gambar = $gambarName;
        $users->level = $request->level;

        $users->save();
        return redirect('/user');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = users::findOrFail($id);

        return view('user.show', compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
