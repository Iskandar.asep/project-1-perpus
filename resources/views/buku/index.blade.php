@extends('layout.master')
@section('judul')
<<<<<<< HEAD
Data Buku
@endsection

@section('content')
<a href="/buku/create" class="btn btn-primary my-2">Tambah</a>

<div class="row">
    @forelse ($datas as $item)
        <div class="col-3">
            <div class="card">
                <img src="{{asset('images_buku/'. $item->cover)}}" class="card-img-top" width="300" height="200" alt="...">
                <div class="card-body">
                <h3>{{$item->judul}}</h3>
                <h5 class="card-title">{{$item->pengarang}}</h5>
                <p class="card-text">{{Str::limit($item->deskripsi, 30)}}</p>
                <form action="buku/{{$item->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/buku/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/buku/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                </form>
                </div>
            </div>
        </div>
    @empty
        <h4>Data Buku Belum Ada</h4>
    @endforelse
</div>
=======
Data Anggota
@endsection

@section('content')

<a href="/buku/create" class="btn btn-success mb-3">Tambah Data</a>

<table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">judul</th>
	  <th scope="col">pengarang</th>
      <th scope="col">penerbit</th>
	  <th scope="col">tahun_terbit</th>
	  <th scope="col">jumlah_buku</th>
	  <th scope="col">deskripsi</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($buku as $data)
	<tr>
		<td>{{$data->judul}}</td>
		<td>{{$data->pengarang}}</td>
		<td>{{$data->penerbit}}</td>
		<td>{{$data->tahun_terbit}}</td>
		<td>{{$data->jumlah_buku}}</td>
		<td>		
			<form action="/buku/{{$data->id}}" method="post">
				<a href="/buku/{{$data->id}}" class="btn btn-info btn-sm" >Detail</a>
				<a href="/buku/{{$data->id}}/edit" class="btn btn-primary btn-sm" >Edit</a>
				@csrf
				@method('DELETE')
			<input type="submit" class="btn btn-danger btn-sm my-1" value="delete">
			</form>
		</td>
	<tr>
	@empty
	<tr>
		<td>Data buku tidak ditemukan</td>
	</tr>
	
	@endforelse
  </tbody>
</table>

>>>>>>> e8ad3920a03029df99da6e48053866b29f18e01a
@endsection