@extends('layout.master')
@section('judul')
<<<<<<< HEAD
Data Buku
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 d-flex align-items-stretch grid-margin">
      <div class="row flex-grow">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Detail Buku : <b>{{$data->judul}}</b> </h4>
              <form class="forms-sample">

                <div class="form-group">
                    <div class="col-md-12">
                        <img width="200" height="200" @if($data->cover) src="{{ asset('images_buku/'.$data->cover) }}" @endif />
                    </div>
                </div>

                <div class="form-group{{ $errors->has('judul') ? ' has-error' : '' }}">
                    <label for="judul" class="col-md-4 control-label">Judul</label>
                    <div class="col-md-12">
                        <input id="judul" type="text" class="form-control" name="judul" value="{{ $data->judul }}" readonly="">
                        @if ($errors->has('judul'))
                            <span class="help-block">
                                <strong>{{ $errors->first('judul') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('npm') ? ' has-error' : '' }}">
                    <label for="isbn" class="col-md-4 control-label">ISBN</label>
                    <div class="col-md-12">
                        <input id="isbn" type="text" class="form-control" name="isbn" value="{{ $data->isbn }}" readonly>
                        @if ($errors->has('isbn'))
                            <span class="help-block">
                                <strong>{{ $errors->first('isbn') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('pengarang') ? ' has-error' : '' }}">
                    <label for="pengarang" class="col-md-4 control-label">Pengarang</label>
                    <div class="col-md-12">
                        <input id="pengarang" type="text" class="form-control" name="pengarang" value="{{ $data->pengarang }}" readonly>
                        @if ($errors->has('pengarang'))
                            <span class="help-block">
                                <strong>{{ $errors->first('pengarang') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('penerbit') ? ' has-error' : '' }}">
                    <label for="penerbit" class="col-md-4 control-label">Penerbit</label>
                    <div class="col-md-12">
                        <input id="penerbit" type="text" class="form-control" name="penerbit" value="{{ $data->penerbit }}" readonly>
                        @if ($errors->has('penerbit'))
                            <span class="help-block">
                                <strong>{{ $errors->first('penerbit') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('tahun_terbit') ? ' has-error' : '' }}">
                    <label for="tahun_terbit" class="col-md-4 control-label">Tahun Terbit</label>
                    <div class="col-md-12">
                        <input id="tahun_terbit" type="number" maxlength="4" class="form-control" name="tahun_terbit" value="{{ $data->tahun_terbit }}" readonly>
                        @if ($errors->has('tahun_terbit'))
                            <span class="help-block">
                                <strong>{{ $errors->first('tahun_terbit') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('jumlah_buku') ? ' has-error' : '' }}">
                    <label for="jumlah_buku" class="col-md-4 control-label">Jumlah Buku</label>
                    <div class="col-md-12">
                        <input id="jumlah_buku" type="number" maxlength="4" class="form-control" name="jumlah_buku" value="{{ $data->jumlah_buku }}" readonly>
                        @if ($errors->has('jumlah_buku'))
                            <span class="help-block">
                                <strong>{{ $errors->first('jumlah_buku') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
                    <label for="deskripsi" class="col-md-4 control-label">Deskripsi</label>
                    <div class="col-md-12">
                        <input id="deskripsi" type="text" class="form-control" name="deskripsi" value="{{ $data->deskripsi }}" readonly="">
                        @if ($errors->has('deskripsi'))
                            <span class="help-block">
                                <strong>{{ $errors->first('deskripsi') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('lokasi') ? ' has-error' : '' }}">
                    <label for="lokasi" class="col-md-4 control-label">Lokasi</label>
                    <div class="col-md-12">
                    <select class="form-control" name="lokasi" disabled="">
                        <option value="rak1" {{$data->lokasi === "rak1" ? "selected" : ""}}>Rak 1</option>
                        <option value="rak2" {{$data->lokasi === "rak2" ? "selected" : ""}}>Rak 2</option>
                        <option value="rak3" {{$data->lokasi === "rak3" ? "selected" : ""}}>Rak 3</option>
                    </select>
                    </div>
                </div>


                <a href="{{route('buku.index')}}" class="btn btn-light pull-right">Back</a>
            </div>
          </div>
        </div>
      </div>
    </div>

</div>
@endsection
=======
Tentang Buku {{$buku->judul}}
@endsection

@section('content')

<table class="table">
    <thead class="thead-light">
        <tr>
          <th scope="col"><h4>Tentang Buku</h4></th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">judul</th>
          <td>: {{$buku->judul}}</td>
        </tr>
        <tr>
          <th scope="row">pengarang</th>
          <td>: {{$buku->pengarang}}</td>
        </tr>
        <tr>
          <th scope="row">penerbit</th>
          <td>: {{$buku->penerbit}}</td>
        </tr>
        <tr>
            <th scope="row">tahun terbit</th>
            <td>: {{$buku->tahun_terbit}}</td>
          </tr>
          <tr>
            <th scope="row">jumlah buku</th>
            <td>: {{$buku->jumlah_buku}}</td>
          </tr>
          <tr>
            <th scope="row">deskripsi</th>
            <td>: {{$buku->deskripsi}}</td>  
          </tr>
      </tbody>
  </table>
  
  <a href="/anggota" class="btn btn-info btn-sm my-2">Kembali</a>
  @endsection
>>>>>>> e8ad3920a03029df99da6e48053866b29f18e01a
