@extends('layout.master')
@section('judul')
Data Buku
@endsection
@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@push('script')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.js"></script>
<script>
  $(function () {
    $("#buku").DataTable();
  });
</script>
@endpush

@section('content')

<table id="buku" class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">No</th>
	  <th scope="col">Cover</th>
	  <th scope="col">Judul</th>
      <th scope="col">Pengarang</th>
	  <th scope="col">Penerbit</th>
	  <th scope="col">Deskripsi</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($data as $key=>$item)
	<tr>
		<td>{{$key +1}}</td>
        <td><img src="{{asset('images_buku/'. $item->cover)}}" class="card-img-top" width="200" height="200" alt="..."></td>
		<td>{{$item->judul}}</td>
		<td>{{$item->pengarang}}</td>
		<td>{{$item->penerbit}}</td>
		<td>{{$item->deskripsi}}</td>
	<tr>
	@empty
	<tr>
		<td>Tidak ada data buku</td>
	</tr>
	
	@endforelse
  </tbody>
</table>

@endsection