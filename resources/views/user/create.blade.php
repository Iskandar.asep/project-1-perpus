@extends('layout.master')
@section('judul')
Halaman User
@endsection

@section('content')

<form action="/user" method="POST" enctype="multipart/form-data">
@csrf
  <div class="form-group">
    <label>Nama</label>
    <input type="text" name="name" class="form-control" placeholder="Ketikan Nama">
  </div>
  @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group">
    <label>Username</label>
    <input type="text" name="username" class="form-control" placeholder="Ketikan Username">
  </div>
  @error('username')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group">
    <label>Email</label>
    <input type="text" name="email" class="form-control" placeholder="Ketikan Email">
  </div>
  @error('email')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group">
    <label>Password</label>
    <input type="password" name="password" class="form-control" placeholder="Ketikan Password">
  </div>
  @error('password')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group">
    <label>Avatar</label>
    <input type="file" name="gambar" class="form-control">
  </div>
  @error('gambar')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group">
    <label>Level</label>
    <select name="level" class="form-control">
      <option value="">---Pilih Level---</option>
        <option value="user">User</option>
        <option value="admin">Admin</option>
</select>
  </div>
  @error('level')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <button type="submit" class="btn btn-primary">Simpan</button>
</form>


@endsection