@extends('layout.master')
@section('judul')
Detail User {{$users->username}}
@endsection

@section('content')

<table class="table">
    <thead class="thead-light">
        <tr>
          <th scope="col"><h4>Detail User</h4></th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody>
        <tr>
            <th scope="row"><img src="{{asset('avatar/'. $users->gambar)}}" alt="" height="100" width="100"></th>
          </tr>
        
        <tr>
          <th scope="row">Nama</th>
          <td>: {{$users->name}}</td>
        </tr>
        <tr>
          <th scope="row">Username</th>
          <td>: {{$users->username}}</td>
        </tr>
        <tr>
          <th scope="row">Email</th>
          <td>: {{$users->email}}</td>
        </tr>
        <tr>
            <th scope="row">Level</th>
            <td>: {{$users->level}}</td>
          </tr>
      </tbody>
  </table>
  
  <a href="/user" class="btn btn-info btn-sm my-2">Kembali</a>
  @endsection