@extends('layout.master')
@section('judul')
Data User
@endsection

@section('content')

<a href="/registrasi" class="btn btn-success mb-3">Tambah Data</a>

<table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
	  <th scope="col">Email</th>
	  <th scope="col">Level</th>
	  <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($users as $key=>$item)
	<tr>
		<td>{{$key +1}}</td>
		<td>{{$item->name}}</td>
		<td>{{$item->email}}</td>
		<td>{{$item->level}}</td>
		<td>		
			<form action="/anggota/{{$item->id}}" method="post">
				<a href="/anggota/{{$item->id}}" class="btn btn-info btn-sm" >Detail</a>
				<a href="/anggota/{{$item->id}}/edit" class="btn btn-primary btn-sm" >Edit</a>
				@csrf
				@method('DELETE')
			<input type="submit" class="btn btn-danger btn-sm my-1" value="delete">
			</form>
		</td>
	<tr>
	@empty
	<tr>
		<td>Tidak ada data user</td>
	</tr>
	
	@endforelse
  </tbody>
</table>

@endsection