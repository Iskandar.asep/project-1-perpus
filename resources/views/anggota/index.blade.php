@extends('layout.master')
@section('judul')
Data Anggota
@endsection

@section('content')

<a href="/anggota/create" class="btn btn-success mb-3">Tambah Data</a>

<table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">No</th>
	  <th scope="col">NPM</th>
      <th scope="col">Nama</th>
	  <th scope="col">Jenis Kelamin</th>
	  <th scope="col">Prodi</th>
	  <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($datas as $key=>$data)
	<tr>
		<td>{{$key +1}}</td>
		<td>{{$data->npm}}</td>
		<td>{{$data->nama}}</td>
		<td>{{$data->jeniskelamin}}</td>
		<td>{{$data->prodi}}</td>
		<td>		
			<form action="/anggota/{{$data->id}}" method="post">
				<a href="/anggota/{{$data->id}}" class="btn btn-info btn-sm" >Detail</a>
				<a href="/anggota/{{$data->id}}/edit" class="btn btn-primary btn-sm" >Edit</a>
				@csrf
				@method('DELETE')
			<input type="submit" class="btn btn-danger btn-sm my-1" value="delete">
			</form>
		</td>
	<tr>
	@empty
	<tr>
		<td>Tidak ada data anggota</td>
	</tr>
	
	@endforelse
  </tbody>
</table>

@endsection