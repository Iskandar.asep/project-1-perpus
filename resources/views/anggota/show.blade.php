@extends('layout.master')
@section('judul')
Detail Anggota {{$data->nama}}
@endsection

@section('content')

<table class="table">
    <thead class="thead-light">
        <tr>
          <th scope="col"><h4>Detail Anggota</h4></th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">ID User</th>
          <td>: {{$data->user_id}}</td>
        </tr>
        <tr>
          <th scope="row">NPM</th>
          <td>: {{$data->npm}}</td>
        </tr>
        <tr>
          <th scope="row">Nama</th>
          <td>: {{$data->nama}}</td>
        </tr>
        <tr>
            <th scope="row">Tempat Lahir</th>
            <td>: {{$data->tempat_lahir}}</td>
          </tr>
          <tr>
            <th scope="row">Tanggal Lahir</th>
            <td>: {{$data->tgl_lahir}}</td>
          </tr>
          <tr>
            <th scope="row">Jenis Kelamin</th>
            <td>: {{$data->jeniskelamin}}</td>  
          </tr>
          <tr>
            <th scope="row">No Telepon</th>
            <td>: {{$data->telepon}}</td>
          </tr>
          <tr>
            <th scope="row">Prodi</th>
            <td>: {{$data->prodi}}</td>
          </tr>
      </tbody>
  </table>
  
  <a href="{{route('anggota.index')}}" class="btn btn-info btn-sm my-2">Kembali</a>
  @endsection