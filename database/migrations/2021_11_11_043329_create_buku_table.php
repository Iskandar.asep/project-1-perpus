<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBukuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buku', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('judul', 255);
            $table->string('isbn', 25);
            $table->string('pengarang', 50);
            $table->string('penerbit', 50);
            $table->integer('tahun_terbit');
            $table->integer('jumlah_buku');
            $table->text('deskripsi');
            $table->enum('lokasi', ['rak_1','rak_2','rak_3']);
            $table->string('cover', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buku');
    }
}
